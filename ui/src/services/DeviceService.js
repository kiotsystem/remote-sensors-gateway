import Api from '@/services/Api'


export default  {
  getDevices () {
    return Api().get('/devices/')
  },
}
