import axios from 'axios'

export default() => {
  const protocol = process.env.VUE_APP_API_PROTOCOL
  const host = process.env.VUE_APP_API_HOST
  const port = process.env.VUE_APP_API_PORT
  const appName = process.env.VUE_APP_API_NAME
  const apiUrl = `${protocol}://${host}:${port}/${appName}`

  return axios.create({
    baseURL: apiUrl,
    withCredentials: false,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  })
}
