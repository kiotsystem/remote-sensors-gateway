import Vue from 'vue'
import Router from 'vue-router'
import Devices from './views/Devices.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'devices',
      component: Devices,
    }
  ]
})
