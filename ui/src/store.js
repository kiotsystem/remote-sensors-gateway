import Vue from 'vue'
import Vuex from 'vuex'
import DeviceService from '@/services/DeviceService'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    devices: [],
  },
  mutations: {
    setDevices: (state, data) => {
      state.devices = data
    },
  },
  actions: {
    async devicesInit({commit}) {
      const { data } = await DeviceService.getDevices()
      commit('setDevices', data)
    },
  }
})
