import time

import pahotoolkit
import schedule

from remote_sensors import app

# we require our callbacks :D
import callbacks  # noqa
import settings
import recovery
import xbee

from log import logger, error_logger


def run():
    """Run the device.

    All calls happen async!
    """

    logger.info('######### Remote Sensors Gateway #########')

    logger.info('##### MQTT ####')
    # set up the MQTT client
    pahotoolkit.start_async(host=settings.MQTT_HOST, port=settings.MQTT_PORT,
                            username=settings.MQTT_USER,
                            password=settings.MQTT_PASSWORD)

    logger.info('#### XBee ####')
    xbee.device.open()

    # add all the remote_sensors callbacks
    app.init(xbee.device)

    schedule.every(15).seconds.do(recovery.do)

    while True:  # live for ever :D
        time.sleep(1)
        try:
            schedule.run_pending()
        except Exception as e:
            error_logger.error(f'failed to execute schedule, {e}')


if __name__ == '__main__':
    try:
        run()
    finally:
        if xbee.device and xbee.device.is_open():
            xbee.device.close()  # close the device
        pahotoolkit.stop_async()  # close the loop
