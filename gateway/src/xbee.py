from digi.xbee.devices import XBeeDevice, RemoteXBeeDevice
from digi.xbee.models.address import XBee64BitAddress, XBee16BitAddress
from digi.xbee.exception import XBeeException
from remote_sensors import Request

import settings

from log import logger


device = XBeeDevice(settings.SERIAL_PORT, settings.BAUD_RATE)


def send_async(addresses: (str, str), request: Request):  # noqa: C901
    """Send async"""
    if not device.is_open():
        logger.error(f'Device is not open...')
        return

    if len(addresses) == 0:
        logger.error(f'Empty addresses, {addresses}')
        return

    try:
        remote_address = XBee64BitAddress.from_hex_string(addresses[0])
    except ValueError:
        logger.error(f'Address is invalid, dropped request: {request}')
        return

    lower_address = XBee16BitAddress.UNKNOWN_ADDRESS
    if len(addresses) == 2:
        try:
            lower_address = XBee16BitAddress.from_hex_string(addresses[1])
        except Exception as e:
            logger.error(f'Failed to get lower address, {addresses}, {e}')

    remote = RemoteXBeeDevice(device, remote_address, lower_address)

    logger.debug(f'Created Remote Device with {remote_address}')

    try:
        payload = request.as_bytearray()
    except Exception as e:
        logger.error(f'failed to dump response: {e}')
        return

    logger.debug(f'{payload}')

    try:
        device.send_data_async(remote, payload)
    except XBeeException as e:
        logger.error(f'Failed to send async data with device, {e}')
    except Exception as e:
        logger.error(f'Unknown: {e}')
    else:
        logger.debug(f'Sent the request {request.method} - {request.uri}'
                     f' to {remote_address}')
