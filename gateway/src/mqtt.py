"""MQTT utility module."""
import functools
import random

from dataclasses import dataclass

from remote_sensors import Request, URI

from log import logger
from settings import MQTT_INTERNAL_NAMESPACE


def get_internal_topic_from(request: Request) -> str:
    """Simple utility function to build a proper topic."""
    topic = f'/{MQTT_INTERNAL_NAMESPACE}/unknown'  # default topic
    if request:
        path = request.uri.path
        if path.startswith('/'):
            path = path[1:]
        topic = f'/{MQTT_INTERNAL_NAMESPACE}/{path}'
    return topic


@dataclass
class StreamRequest:
    """Request for streams in the MQTT channel."""

    uri: str
    state: bool
    addresses: (str, str) = (None, None)
    method: str = 'GET'

    def has_addresses(self) -> bool:
        return 0 < len(self.addresses) <= 2 and self.addresses[0] is not None

    def as_remote_sensors_request(self, transaction: int = 0) -> Request:
        """Return a RemoteSensor API Request.

        :returns remote_sensors.Request:
        """
        if not transaction or not (0 < transaction <= 255):
            transaction = random.randint(1, 255)

        if self.state:
            connection = 'STREAM'
        else:
            connection = 'CLOSE'

        uri = URI(self.uri)
        request = Request(self.method, connection=connection,
                          transaction=transaction, uri=uri)
        return request


def stream_request(none_on_failure: bool = True):
    """Stream Request or nothing, maybe."""
    def decorator(fn):
        @functools.wraps(fn)
        def execute(payload: dict, *args, **kwargs):
            try:
                stream_req = StreamRequest(**payload)
            except Exception as e:
                logger.error(f'Failed to parse the Stream Request, {e}')
                if none_on_failure:
                    return None
            else:
                return fn(stream_request=stream_req,
                          payload=payload, *args, **kwargs)
        return execute
    return decorator
