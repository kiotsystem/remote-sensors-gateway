import redis

import settings


cache = redis.StrictRedis(host=settings.REDIS_HOST,
                          port=settings.REDIS_PORT,
                          decode_responses=True)
