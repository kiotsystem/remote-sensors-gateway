import json

from datetime import datetime

import remote_sensors

from remote_sensors import Request, Response
from remote_sensors.xbee import SensorRequest, SensorResponse
from remote_sensors.log import logger, error_logger

import pahotoolkit
import mqtt
import sensors
import settings
import xbee as device

from cache import cache


@remote_sensors.bind_to_device()
@remote_sensors.request()
def access_log(request: Request, *args, **kwargs):
    """Access logs."""
    logger.info(f'Request: {request.method} - {request.uri}')


@remote_sensors.bind_to_device()
@remote_sensors.request(method='REGISTRATION', pass_message=True)
def handle_registration(request: Request, sensor_message: SensorRequest,
                        *args, **kwargs):
    """Handles the registration request."""
    response = Response(status='ACK',
                        transaction=request.transaction)

    # do something about it :D
    payload = {
        'device': sensor_message.as_dict(),
        'request': request.as_dict(),
        'response': response.as_dict(),
        'completed': False,
    }
    topic = f'/{settings.MQTT_INTERNAL_NAMESPACE}/registrations'
    pahotoolkit.client.publish(topic, json.dumps(payload))

    address, small_address = sensor_message.addresses
    cache.sadd(f'{address}:resources', str(request.uri))
    cache.set(f'{address}:16bitaddress', str(small_address))

    logger.debug(f'Replying with {response.status} to {sensor_message.addresses}')

    return response


@remote_sensors.bind_to_device()
@remote_sensors.request(method='END_REGISTRATION', pass_message=True)
def handle_registration_end(request: Request, sensor_message: SensorRequest,
                            *args, **kwargs):
    """Handles the end of registration."""
    response = Response(status='ACK',
                        transaction=request.transaction)

    # do something about it :D
    payload = {
        'device': sensor_message.as_dict(),
        'request': request.as_dict(),
        'response': response.as_dict(),
        'completed': True,
    }
    topic = f'/{settings.MQTT_INTERNAL_NAMESPACE}/registrations'
    pahotoolkit.client.publish(topic, json.dumps(payload))

    address, small_address = sensor_message.addresses
    cache.sadd(f'{address}:resources', str(request.uri))
    cache.set(f'{address}:16bitaddress', str(small_address))

    cache.set(f'{address}:registered', str(True))
    cache.set(f'{address}:timestamp', str(datetime.now().timestamp()))
    cache.set(f'{address}:last-alive', str(datetime.now().timestamp()))

    cache.sadd('devices', str(address))

    logger.info(f'Device ({sensor_message.addresses}) has finished registration')

    return response


@remote_sensors.bind_to_device()
@remote_sensors.response(status='SEND', pass_message=True)  # we are not interested in the ACK
def forward_streams(response: Response, sensor_message: SensorResponse,
                    *args, **kwargs):
    """Forward the stream responses."""
    try:
        payload = response.as_dict()
    except Exception as e:
        logger.error(f'Failed to convert response to dict, {e}')
        raise e

    address, small_address = sensor_message.addresses
    request = sensors.get_stream_from(address, response.transaction)
    topic: str = mqtt.get_internal_topic_from(request)

    cache.set(f'{address}:last-alive', str(datetime.now().timestamp()))

    pahotoolkit.client.publish(topic, json.dumps(payload))
    logger.debug(f'Published to {topic}')


@pahotoolkit.subscribe(f'/{settings.MQTT_INTERNAL_NAMESPACE}/streams')
@pahotoolkit.json_message()
@mqtt.stream_request()
def listen_for_stream_requests(stream_request: mqtt.StreamRequest, *args, **kwargs):
    """Handles a request to start or stop a stream."""
    request = stream_request.as_remote_sensors_request()
    logger.debug(f'Got from topic: {request.uri} {request.method}')

    # get the device to request
    addresses = stream_request.addresses
    if not stream_request.has_addresses():
        addresses = sensors.get_device_for(request.uri)

    if len(addresses) == 0:
        error_logger.error(f'No addresses found, discarding stream request')
        return

    address = addresses[0]
    registered_stream_request: Request = sensors.stream_for(address, request.uri)
    device_is_active = sensors.is_device_alive(address, 10)
    if registered_stream_request and device_is_active:
        logger.info(f'A stream request was already sent to {address}:{request.uri}')
        return

    if not registered_stream_request:
        cache.sadd(f'{address}:streams', json.dumps(request.as_dict()))
        logger.info(f'Added {request.uri} to streams')

    if registered_stream_request:
        device.send_async(addresses, registered_stream_request)
    else:
        device.send_async(addresses, request)
    logger.info(f'Sent stream request to {addresses}')
