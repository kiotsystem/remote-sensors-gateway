import json

from remote_sensors import Request, URI

import sensors
import xbee

from cache import cache
from log import logger


def do(toleration: int = 30):
    """Execute the recovery of all the registered Streams."""
    devices = sensors.devices()

    for device in devices:
        streams = cache.smembers(f'{device}:streams')
        if not streams:
            logger.debug(f'Device {device} has no streams, skipping')
            continue

        is_alive = sensors.is_device_alive(device, toleration)
        if is_alive:
            logger.info(f'Device {device} is still alive, skipping')
            continue

        logger.info(f'Device {device} seems to be missing...')

        for stream in streams:
            logger.debug(f'Attempting to send stream to {device}')
            if isinstance(stream, str):
                stream = json.loads(stream)
            resend_stream(device, stream)


def resend_stream(device: str, stream: dict):
    """Resends the Stream request."""
    uri = URI(**stream.get('uri'))
    request = Request(**stream)
    if uri:
        request.uri = uri

    small_address = cache.get(f'{device}:16bitaddress')
    xbee.send_async((device, small_address), request)
    logger.info(f'Sent stream request to {device}:{small_address}:{uri}')
