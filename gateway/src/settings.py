import os


SERIAL_PORT = os.environ['SERIAL_PORT']

BAUD_RATE = int(os.environ['BAUD_RATE'])

MQTT_HOST = os.getenv('MQTT_HOST')

MQTT_PORT = int(os.getenv('MQTT_PORT', 1883))

MQTT_PROTOCOL = os.getenv('MQTT_PROTOCOL', 'mqtt')

MQTT_USER = os.getenv('MQTT_USER')

MQTT_PASSWORD = os.getenv('MQTT_PASSWORD')

MQTT_INTERNAL_NAMESPACE = os.getenv('MQTT_INTERNAL_NAMESPACE')

MQTT_NAMESPACE = os.getenv('MQTT_NAMESPACE')

REDIS_HOST = os.getenv('REDIS_HOST')

REDIS_PORT = int(os.getenv('REDIS_PORT'))
