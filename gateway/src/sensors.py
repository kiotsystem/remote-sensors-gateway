import json

from datetime import datetime

from remote_sensors import Request, URI

from cache import cache
from log import error_logger


def devices():
    registered_devices = cache.smembers('devices')
    return registered_devices


def is_device_alive(device: str, tolerance_secs: int = 300):
    last_alive = cache.get(f'{device}:last-alive')
    if not last_alive:
        return False

    try:
        last_alive = datetime.fromtimestamp(float(last_alive))
    except Exception as e:
        error_logger.error(f'Failed to get timestamp {e}')
        return False

    now = datetime.now()

    delta = now - last_alive

    return 0 <= delta.total_seconds() <= tolerance_secs


def resources_from(device: str):
    return cache.smembers(f'{device}:resources')


def get_device_for(uri: str):
    registered_devices = devices()
    for device in registered_devices:
        resources = resources_from(device)
        for resource in resources:
            if uri == resource or uri == f'{resource}/':
                small_address = cache.get(f'{device}:16bitaddress')
                return device, small_address
    return None, None


def get_stream_from(device: str, transaction: int) -> Request:
    streams = cache.smembers(f'{device}:streams')
    for stream in streams:
        st = json.loads(stream)
        if st.get('transaction') == transaction:
            return _from_stream(st)
    return None


def stream_for(device: str, uri: str) -> Request:
    streams = cache.smembers(f'{device}:streams')
    for stream in streams:
        request = _from_stream(json.loads(stream))
        if request.uri == uri:
            return request
    return False


def _from_stream(stream: dict) -> Request:
    request = Request(**stream)
    if stream.get('uri'):
        raw_uri = stream.get('uri')
        uri = URI(**raw_uri)
        request.uri = uri
    return request
