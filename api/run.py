from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler

import settings

from app import app
from log import logger


server = WSGIServer((settings.HOSTNAME, settings.PORT),
                    app,
                    handler_class=WebSocketHandler)


if __name__ == '__main__':
    try:
        server.serve_forever()
    finally:  # clean up
        logger.info('Cleaning up')
