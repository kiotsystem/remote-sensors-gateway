import json

from datetime import datetime

from bottle import request, response

import cache

from app import app
from log import access_logger


@app.hook('after_request')
def after_request_hook():
    response.headers['Access-Control-Allow-Origin'] = '*'

    now = datetime.now().isoformat()
    access_logger.info(f'{request.remote_addr} - - [{now}] '
                       f'"{request.method} {request.path} HTTP/1.1" '
                       f'{response.status_code} -')


@app.get('/devices/')
@app.get('/remote-sensors-api/devices/')
def get_devices():
    devices = cache.devices()

    response.content_type = 'application/json'
    return json.dumps(devices)
