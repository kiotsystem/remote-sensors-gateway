"""Logger module."""
import logging.config
import sys

import settings


LOGGING_CONFIG_DEFAULTS = dict(
    version=1,
    disable_existing_loggers=False,

    loggers={
        'main': {
            'level': settings.LOGGING_LEVEL,
            'handlers': ['console'],
        },
        'accesslog': {
            'level': 'INFO',
            'handlers': ['accesslog'],
        },
        'error': {
            'level': 'INFO',
            'handlers': ['error_console'],
            'propagate': True,
            'qualname': 'error'
        },
    },
    handlers={
        'accesslog': {
            'class': 'logging.StreamHandler',
            'formatter': 'accesslog',
            'stream': sys.stdout,
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'generic',
            'stream': sys.stdout
        },
        'error_console': {
            'class': 'logging.StreamHandler',
            'formatter': 'generic',
            'stream': sys.stderr
        },
    },
    formatters={
        'accesslog': {
            'format': '%(message)s',
            'class': 'logging.Formatter',
        },
        'generic': {
            'format': '[%(thread)d - %(module)s - %(funcName)s] [%(levelname)s] %(message)s',  # noqa
            'class': 'logging.Formatter',
        },
    }
)

logging.config.dictConfig(LOGGING_CONFIG_DEFAULTS)

logger = logging.getLogger('main')

access_logger = logging.getLogger('accesslog')

error_logger = logging.getLogger('error')
