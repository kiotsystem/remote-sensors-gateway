import os


HOSTNAME = os.getenv('HOSTNAME')

PORT = int(os.getenv('PORT', 80))

LOGGING_LEVEL = os.getenv('LOGGING_LEVEL', 'INFO')

REDIS_HOST = os.getenv('REDIS_HOST')

REDIS_PORT = int(os.getenv('REDIS_PORT'))
