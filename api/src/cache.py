import json

from datetime import datetime

import redis

import settings

from log import logger


cache = redis.StrictRedis(host=settings.REDIS_HOST,
                          port=settings.REDIS_PORT,
                          decode_responses=True)


def devices() -> list:
    registered_devices = cache.smembers('devices')
    logger.info(f'Got {len(registered_devices)} devices')

    def to_iso_date(timestamp: str):
        my_date = datetime.fromtimestamp(float(timestamp))
        return my_date.isoformat()

    devs = list()
    for device in registered_devices:
        dev = dict()
        dev['address'] = device
        dev['small_address'] = cache.get(f'{device}:16bitaddress')
        dev['registered'] = cache.get(f'{device}:registered')

        dev['resources'] = list(cache.smembers(f'{device}:resources'))

        _streams = cache.smembers(f'{device}:streams')
        streams = list()
        for stream in _streams:
            streams.append(json.loads(stream))
        dev['streams'] = streams

        dev['timestamp'] = to_iso_date(cache.get(f'{device}:timestamp'))
        dev['last_alive'] = to_iso_date(cache.get(f'{device}:last-alive'))
        devs.append(dev)

    return devs
